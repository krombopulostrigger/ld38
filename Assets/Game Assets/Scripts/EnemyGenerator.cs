﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

	public long m_spawnTimer = 0;
	public long m_spawnRadius = 0;
	public int m_spawnCap = 0;
	public GameObject m_prefab;
	public Transform m_spawnLocation;

	public GameObject m_goal;

	protected long m_lastSpawnTime;
	protected int m_spawnCount = 0;

    protected int m_currentSpawnAttempt;
    public int m_maxSpawnAttempt;

	// Use this for initialization
	void Start ()
	{
		m_lastSpawnTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        m_maxSpawnAttempt = 10;
    }
	
	// Update is called once per frame
	void Update () 
	{
		long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
		if (currentTime-m_lastSpawnTime > (m_spawnTimer*1000))
		{
			//Spawn something
			tryToSpawn();
			m_lastSpawnTime = currentTime;

		}
	}
		
	private void tryToSpawn()
	{
		if(m_spawnCount >= m_spawnCap)
		{
            //destroy spawner
            Destroy(this.gameObject);
            return;
		}

		Vector3 validSpawnLocation = new Vector3 ();
		bool foundSpawn = false;

		for(long i = 0; i <= m_spawnRadius; ++i)
		{
			for (int angle = 0; angle <= 360; angle += 10) 
			{
				if (trySpawnLocation (angle, i, ref validSpawnLocation)) 
				{
					foundSpawn = true;
                    m_currentSpawnAttempt = 0;
					break;
				}
                else
                {
                    m_currentSpawnAttempt += 1;
                    if(m_currentSpawnAttempt > m_maxSpawnAttempt)
                    {
                        Destroy(this.gameObject);
                    }
                }
			}
			if (foundSpawn) 
			{
				break;
			}
		}

		if (foundSpawn) 
		{
			GameObject enemy = Instantiate (m_prefab, validSpawnLocation, Quaternion.identity);
			//TODO Set goal as end objective
			m_spawnCount++;
		}
	}

	//Given an angle and a radius, check if the position is occupied given a radius of 1.
	private bool trySpawnLocation(int angle, long radius, ref Vector3 spawnPoint)
	{
		bool validSpawn = true;

		Vector3 spawnDirection = Quaternion.AngleAxis(angle, m_spawnLocation.up) * m_spawnLocation.forward;

		Vector3 potentialSpawn = m_spawnLocation.position + spawnDirection.normalized * radius;

		Collider[] hitColliders = Physics.OverlapSphere (potentialSpawn, 1);

		//check for enemies already existing
		foreach(Collider other in hitColliders) 
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("Character")) 
			{
				validSpawn = false;
				break;
			}
		}

		//Check for floor
		if (validSpawn) 
		{
			Ray groundRay = new Ray(potentialSpawn, new Vector3(0f, -1f, 0f));
			RaycastHit groundHit;
			if (Physics.Raycast (groundRay, out groundHit, 100f)) {
				validSpawn = groundHit.collider.gameObject.layer == LayerMask.NameToLayer ("Ground");
			} 
			else 
			{
				validSpawn = false;
			}
		}

		if (validSpawn) 
		{
			spawnPoint = potentialSpawn;
		}

		return validSpawn;
	}
}
