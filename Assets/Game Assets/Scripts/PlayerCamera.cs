﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    public GameObject player;
    public Camera playerCamera;
    public Vector3 playerCamOffset = new Vector3(0, 50, 0);


    // Use this for initialization
    void Start ()
    {
        playerCamera.transform.position = player.transform.position + playerCamOffset;
        playerCamera.transform.rotation = Quaternion.Euler(90,0,0);
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        Rigidbody playerRB = player.GetComponentInChildren<Rigidbody>();
        if (playerRB != null)
        {
            playerCamera.transform.position = playerRB.position + playerCamOffset;
        }
    }
}
