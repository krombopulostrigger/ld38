﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapGenerator : MonoBehaviour {
    public int MIN_WIDTH=25; public int MIN_HEIGHT=50;
    public int MAX_WIDTH=25; public int MAX_HEIGHT=50;
    public float MAX_NATURAL_BLDG_PRCNT = 0.01f;

    public int tileXScale = 1; public int tileYScale = 1; public int tileZScale = 1;
    public int tileYOffset;

    private int width, height;
    private int[] startPos;
    private static int levelNo;
    private GameObject gameMap;
    private GameObject[] tilesInMap;
    //private GameObject[] buildingsOnMap;
    public GameObject[] buildingsOnMap;
    private GameObject autoBldgs;

    public GameObject playerObject;
    public GameObject playerSpawnerPrefab;

    //buildings
    public GameObject basicBarricade;

	// Use this for initialization
	void Start () {
        gameMap = new GameObject();
        gameMap.name = "GameMap";
        levelNo = 0;
        startPos = new int[2]; startPos[0] = -1; startPos[1] = -1;
        createMap(50, 50); //start with a 50x50 map
        
	}

    void createMap(int widthIn, int heightIn)
    {
        width = (widthIn > 9) ? widthIn : 10;
        height = (heightIn > 9) ? heightIn : 10;

        tilesInMap = new GameObject[width * height];
        buildingsOnMap = new GameObject[width * height];
        GameObject tmpCube;

        //generate the map and draw it in the scene
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                //generate the new primitive and put it in the scene
                tmpCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                tmpCube.transform.parent = gameMap.transform;
                tmpCube.transform.localScale = new Vector3(tileXScale, tileYScale, tileZScale);
                tmpCube.transform.position = new Vector3(i * tileXScale, tileYOffset, j * tileZScale);
                tmpCube.name = "MapTile[" + i + "," + j + "]";
                tmpCube.tag = "GroundTile";
				tmpCube.layer = LayerMask.NameToLayer ("Ground");
                //add the primitive to our tile map
                tilesInMap[(i * width) + j] = tmpCube;
            }
        }

        //choose the player starting positions
        System.Random rnd = new System.Random();
        startPos[0] = rnd.Next(0, width);
        startPos[1] = rnd.Next(0, height);
        GameObject playerSpawner = Instantiate(playerSpawnerPrefab);
        playerSpawner.name = "PlayerStartTile";
        playerSpawner.transform.position = new Vector3(startPos[0] * tileXScale, tileYOffset+(tileYScale), startPos[1] * tileZScale);
        //set the player's starting position somehow?
        //Rigidbody prb = playerObject.GetComponentInChildren<Rigidbody>();
        //prb.transform.position = new Vector3(startPos[0] * tileXScale, 3.5f, startPos[1] * tileZScale);
        //playerObject.transform.position = new Vector3(startPos[0] * tileXScale, 3.5f, startPos[1] * tileZScale);
        buildingsOnMap[width * startPos[1] + startPos[0]] = playerSpawner;

        //add buildings
        autoBldgs = new GameObject();
        autoBldgs.name = "GeneratedBldgs";
        autoBldgs.transform.parent = gameMap.transform;
        int maxBldgs = (int)Mathf.Floor( (width*height)*MAX_NATURAL_BLDG_PRCNT );
        int bldgsToGen = rnd.Next(0,maxBldgs);
        int bldgXPos, bldgYPos;
        for(int i=0; i< bldgsToGen; i++)
        {
            bldgXPos = rnd.Next(0, width); bldgYPos = rnd.Next(0, height); int maxAttempts = 5;
            while (buildingsOnMap[bldgYPos*width+bldgXPos]!=null && maxAttempts-- >= 0)
            {
                bldgXPos = rnd.Next(0, width + 1); bldgYPos = rnd.Next(0, height + 1);
                maxAttempts--;
            }
            if (maxAttempts > 0)
            {
                //choose and assign the building to build
                int numBldgTypes = 1; int bldgToGen = rnd.Next(0, numBldgTypes);
                switch (bldgToGen)
                {
                    case 0:
                        GameObject myObj = Instantiate(basicBarricade, transform.position, transform.rotation);
                        myObj.transform.position = new Vector3(bldgXPos*tileXScale + tileXScale/2, tileYScale+tileYOffset, bldgYPos*tileZScale+ tileZScale/2);
                        myObj.name = "BasicBarricade_auto"+i;
                        myObj.tag = "BasicBarricade";
                        myObj.transform.parent = autoBldgs.transform;
                        buildingsOnMap[bldgYPos * width + bldgXPos] = myObj;

                        break;
                    default:
                        //error handling
                        break;
                }
            }
        }
    }

    void tearDownMap()
    {

    }

    public int[] getPlayerStartTile()
    {
        return startPos;
    }

    public int[] getMapDims()
    {
        return new int[5]{ width, height, tileXScale, tileYScale, tileZScale };
    }

    public GameObject getObjAtAddr(int xIn, int yIn)
    {
        return buildingsOnMap[(yIn * width) + xIn];
    }

    public void putGameObjAtAddr(GameObject objIn, int xIn, int yIn)
    {
        if(xIn >= width || yIn >= height || objIn == null) { return; } //don't do anything if input is invalid
        buildingsOnMap[(yIn * width) + xIn] = objIn;
    }
}
