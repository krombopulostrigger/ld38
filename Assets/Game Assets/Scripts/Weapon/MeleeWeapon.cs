﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * This will be the generic Melee Weapon class
 * All weapons should have:
 * 1. Arc
 * ??
 * 2. Knockback?
 * 
 * 
**/
public class MeleeWeapon : Weapon
{
    public float m_arc;
    private bool m_swing = false;
    // Use this for initialization
    
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        if (!m_swing)
        {
            GetInput();
        }
        long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        if (currentTime - m_lastUse > (m_rateOfUse * 1000) && m_swing)
        {
            Use();
        }
        else
        {
            m_swing = false;
        }
    }


    public override void Use()
    {
		//Base calls attack sound
		base.Use ();

        RaycastHit[] enemiesHit;
        enemiesHit = Physics.SphereCastAll(transform.position, m_range, new Vector3(0f, -1f, 0f));
        foreach (RaycastHit c in enemiesHit)
        {
            Vector3 vectorToCollider = (c.collider.transform.position - m_player.transform.position);
            // 180 degree arc, change 0 to 0.5 for a 90 degree "pie"
            float test = Vector3.Dot(vectorToCollider, m_player.transform.transform.forward);
            if (test > 0)
            {
                if ((c.collider.gameObject.layer == 9 || c.collider.gameObject.layer == 11))
                {
                    if (!c.collider.gameObject.CompareTag("Untagged") && !c.collider.gameObject.CompareTag(m_player.tag))
                    {
                        if (c.collider.gameObject.CompareTag("Enemy"))
                        {
                            c.collider.gameObject.GetComponent<EnemyController>().reduceHealth(m_damage);
                        }
                        if (c.collider.gameObject.CompareTag("Player"))
                        {
                            c.collider.gameObject.GetComponent<PlayerController>().reduceHealth(m_damage);
                        }
                        if (c.collider.gameObject.layer == 11)
                        {
                            c.collider.gameObject.GetComponent<Building>().damage(m_damage);
                        }
                    }

                }
            }
        }


        m_lastUse = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        m_swing = false;
    }

    void GetInput()
    {
        m_swing = Input.GetButtonDown("Fire1");
    }

    public void Swing()
    {
        m_swing = true;
    }

}
