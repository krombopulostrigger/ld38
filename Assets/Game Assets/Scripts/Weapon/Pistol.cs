﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun
{
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        m_damage = 1;
        m_range = 100f;
        m_rateOfUse = 0.1f;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }



}
