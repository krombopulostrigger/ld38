﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * This will be the generic Weapon class
 * All weapons should have:
 * 1. Damage
 * 2. Rate of Fire (or swing)
 * 3. Sound effect
 * 4. Range
 * 
 * 
 * Note: the gun is the actual attack for the hero so only enemy classes
 * need an attack rate
**/
public class Weapon : MonoBehaviour {

    public AudioSource m_audioSource;
    public float m_range;
    public float m_damage;
    public long m_lastUse;
    public float m_rateOfUse;
    public GameObject m_player;
	protected AudioClip m_attackSound;

    // Use this for initialization
	public virtual void Start ()
    {
		m_audioSource = GetComponentInParent<AudioSource>();
        m_lastUse = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }

    // Update is called once per frame
    public virtual void Update()
    {

    }


    public virtual void Use()
    {
		if (m_attackSound != null) {
			m_audioSource.PlayOneShot (m_attackSound);
		} else {
			m_audioSource.Play ();
		}
    }

	public void setAttackSound(AudioClip attackSound)
	{
		m_attackSound = attackSound;
	}
}
