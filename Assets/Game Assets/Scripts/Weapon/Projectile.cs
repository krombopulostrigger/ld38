﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * This will be the generic Projectile class
 * All projectiles should have:
 * 1. Damage Modifer
 * 2. Projectile Speed Modifer
 * 3. Sound effect ??
 * 4. Max Range 
 * 5. Projectile Size
 * 6. Weapon is is being fired from
 * 
 * 
 * Note: the gun is the actual attack for the hero so only enemy classes
 * need an attack rate
**/
public class Projectile : MonoBehaviour
{
    public Gun m_gun;

	protected Vector3 m_startPosition;
	public float m_distanceModifer;
	public float m_damageModifier;
    public float m_speedModifer;
    public float m_projectileSize;
    // Use this for initialization
    void Start () 
	{
		m_startPosition = transform.position;
        transform.localScale = new Vector3(m_projectileSize, m_projectileSize, m_projectileSize);
	}

	// Update is called once per frame
	void Update () 
	{
        float distanceTraveled = Math.Abs(Vector3.Distance(m_startPosition, transform.position));
        if (distanceTraveled > m_distanceModifer * m_gun.m_range)
        {
            Destroy(gameObject);
        }

    }

    void OnCollisionEnter(Collision Collection)
	{
		if (Collection.gameObject.CompareTag("Enemy")&& 
			Collection.gameObject.layer != LayerMask.NameToLayer("Dead")) 
		{
            // Issue where enmies get hit multiple (two usually) times with a projectile.
			Collection.gameObject.GetComponent<EnemyController>().reduceHealth (m_gun.m_damage * m_damageModifier);
			Destroy (gameObject);
		}
		else if (Collection.gameObject.layer == LayerMask.NameToLayer("Construct"))
        {
			if (!Collection.gameObject.CompareTag ("Goal")) {
				Building tmpBldg = Collection.gameObject.GetComponent<Building>();
				tmpBldg.damage((int)m_damageModifier);
			}
            Destroy(gameObject);
        }
        else if(Collection.gameObject.layer != LayerMask.NameToLayer("Projectile") && 
			Collection.gameObject.layer != LayerMask.NameToLayer("Dead"))
		{
			//assume it collided with environment
			Destroy (gameObject);
		}
	}

}
