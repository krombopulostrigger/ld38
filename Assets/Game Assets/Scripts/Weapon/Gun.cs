﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * This will be the generic Gun class
 * Inherits from the Weapon Class 
 * All weapons should have:
 * 1. Max Clip Size
 * 2. Current Clip Size
 * 3. Total Ammo
 * 4. Reload Speed
 * 5. A Projectile to use
 * 6. Projectile speed
 * 
 * 
 * Note: the gun is the actual attack for the hero so only enemy classes
 * need an attack rate
**/
public class Gun : Weapon
{

    public Transform m_barrel;
    public GameObject m_projectile;
    public PlayerController m_playerController;

    public int m_maxClipSize;
    public int m_currentClipSize;
    public int m_totalAmmo;
    public float m_reloadSpeed;

    private long m_reloadTime;
    public bool m_reloading = false;

    // Use this for initialization
    public override void Start()
    {
		m_currentClipSize = m_maxClipSize;
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            if (m_currentClipSize != 0)
            {
                if (currentTime - m_lastUse > (m_rateOfUse * 1000) && currentTime - m_reloadTime > (m_reloadSpeed * 1000))
                {
                    Use();
                }
            }
            else if (currentTime - m_reloadTime > (m_reloadSpeed * 1000))
            {
                Reload();
            }
        }
    }

    public override void Use()
	{
		//Base calls attack sound
		base.Use ();
        //Create Bullet
        GameObject projectile = Instantiate(m_projectile, m_barrel.position, Quaternion.identity);
        //set which gun fired the projectile
        Projectile firedProjectile = projectile.GetComponent<Projectile>();
        firedProjectile.m_gun = this;

        projectile.GetComponent<Rigidbody>().AddForce(m_barrel.up * firedProjectile.m_speedModifer);

        m_currentClipSize -= 1;

        //long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        //sets lastUse to current time
        m_lastUse = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

    }

    public virtual void Reload()
    {
        if (m_totalAmmo > m_maxClipSize)
        {
            int temp = m_currentClipSize;
            m_currentClipSize = m_maxClipSize;
            m_totalAmmo -= m_maxClipSize - temp;
        }
        else if (m_totalAmmo > 0)
        {
            m_currentClipSize = m_totalAmmo;
            m_totalAmmo = 0;
        }
        else
        {
            //No AMMO
            return;
        }
        m_reloadTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        //throw something on the screen
        m_playerController.ReloadAnimate(m_reloadSpeed);
    }

}

