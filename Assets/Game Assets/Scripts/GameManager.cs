﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public bool m_isGameOver = false;

    public Image m_uiGameOverImage;
    public GameObject m_uiGameOverlay;
    public SpawnerSpawner m_spawnerSpawner;
    public GameObject m_prefabToSpawn;

    public GameObject m_pauseMenu;

    protected int m_spawnCount = 0;

    public float m_waveDelay = 1;
    private int m_waveNumber;
    protected long m_waveStart;

    public GameObject m_waveUI;
    // Use this for initialization
    void Start()
    {
        m_waveNumber = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_isGameOver)
        {
            GameOverImage();
            GameOverOverlay();
        }
        if (isWaveFinished())
            StartWave();

        if (Input.GetButtonDown("Cancel"))
        {
            if (m_pauseMenu.activeSelf)
            {
                m_pauseMenu.SetActive(false);
            }
            else
            {
                m_pauseMenu.SetActive(true);
            }
        }
    }

    public void SetGameOver()
    {
        m_isGameOver = true;
    }

    private void GameOverImage()
    {
        Color c = m_uiGameOverImage.color;
        c.a = 0.75f;
        m_uiGameOverImage.color = c;
    }

    private void GameOverOverlay()
    {
        m_uiGameOverlay.SetActive(true);
    }

    public void MainMenu(string mainMenu)
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void Restart(string restartScene)
    {
        SceneManager.LoadScene(restartScene);
    }

    public void ClosePause()
    {
        m_pauseMenu.SetActive(false);
    }

    public void StartWave()
    {
        m_waveNumber += 1;

        m_waveUI.GetComponentInChildren<Text>().text = "Wave   " + m_waveNumber.ToString();
        m_waveUI.SetActive(true);

        m_spawnerSpawner.m_spawnCap = m_waveNumber % 4;
        m_spawnerSpawner.ModifyPrefab(MakeEnemiesHarder(m_waveNumber));
        StartCoroutine(m_spawnerSpawner.SetEnable(true, m_waveDelay, m_waveUI));
        m_waveStart = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }

    public bool isWaveFinished()
    {
        long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        if (currentTime - m_waveStart > (5000 * m_waveDelay))
        {
            if (GameObject.Find("DumbZombieWithAnAxe(Clone)") == null)
            {
                return true;
            }
        }
        return false;
    }

    private GameObject MakeEnemiesHarder(int wave_number)
    {
        int spawntimer = wave_number % 4;
        int spawncap = wave_number % 4 + 1;
        m_prefabToSpawn.GetComponent<EnemyGenerator>().m_spawnTimer = spawntimer;
        m_prefabToSpawn.GetComponent<EnemyGenerator>().m_spawnCap = spawncap;
        return m_prefabToSpawn;
    }
}
