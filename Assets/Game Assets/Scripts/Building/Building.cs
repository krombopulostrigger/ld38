﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public abstract class Building : MonoBehaviour {

    protected float MAX_HEALTH;
    public float health;
    public bool isDestructible;
	protected bool m_isDead = false;

	public long m_hitDelay = 10;
	private long m_lastHit;
	private bool m_canHit = true;

	public AudioClip m_destructionNoise;
	public AudioClip m_hitNoise;

	protected AudioSource m_audioSource;
	private Renderer m_rend;
	private Color m_normalColor;
    public Slider m_healthSlider;

    public bool m_failOnDistruction = false;


	// Use this for initialization
	public virtual void Start () {
		health = MAX_HEALTH;
		m_audioSource = GetComponent<AudioSource>();
		m_rend = GetComponent<Renderer> ();
		m_normalColor = m_rend.material.color;
        if (m_healthSlider != null)
        {
            m_healthSlider.maxValue = MAX_HEALTH;
            m_healthSlider.value = health;
        }
	}

	// Update is called once per frame
	public virtual void Update()
	{
		if (!m_canHit)
		{
			long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			if (currentTime-m_lastHit > m_hitDelay) 
			{
				m_canHit = true;
			}
		}
	}
	
	public void damage(float damageToDo)
    {

		if(isDestructible && m_canHit)
		{
			health -= damageToDo;
            if (m_healthSlider != null)
            {
                m_healthSlider.value = health;
            }

                m_audioSource.PlayOneShot (m_hitNoise);
			//added isDead to prevent extra dieing on collisions after death
			if (health <= 0 && !m_isDead) 
			{
				m_isDead = true;
				kill ();
			}
			m_lastHit = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			m_canHit = false;
		}
    }

    public void repair(int healthToRepair)
    {
        if (!isDestructible) { return; }
        health = (health + healthToRepair > MAX_HEALTH) ? MAX_HEALTH : health + healthToRepair;
    }

    public virtual void kill()
    {
		m_audioSource.PlayOneShot (m_destructionNoise);
		updateLayers (gameObject, LayerMask.NameToLayer("Dead"));

		Destroy (gameObject, 1f);
    }//maybe use for things like exploding barrels

    public float getHealth() { return isDestructible? health : -1; }

	public void updateLayers(GameObject obj, int newLayer)
	{
		obj.layer = newLayer;

		foreach (Transform child in obj.transform) {
			updateLayers (child.gameObject, newLayer);
		}
	}
}
