﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : Building {

	// Use this for initialization
    public GameManager m_gameManager;
	public override void Start () {
		MAX_HEALTH = 10;
		base.Start ();
        m_failOnDistruction = true;
        m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

	public override void kill()
	{
		//GAME OVER
		base.kill ();
        m_gameManager.SetGameOver();
	}
}
