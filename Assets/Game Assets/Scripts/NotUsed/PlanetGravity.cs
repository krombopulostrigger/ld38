﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlanetGravity : MonoBehaviour
{

    public List<GameObject> objects;
    public GameObject planet;
    public Collider[] colliderspub;
    private CubeSpherePlanet planetCreator;

    public float gravitationalPull = 10f;
    public float atmosphereRadius = 20f;
    private float gravityRadius;

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(planet.transform.position, gravityRadius);
    }

    private void Start()
    {
        planetCreator = planet.GetComponent<CubeSpherePlanet>();
        gravityRadius = planetCreator.radius + atmosphereRadius;
    }


    void FixedUpdate()
    {
        {
            colliderspub = Physics.OverlapSphere(planet.transform.position, gravityRadius);
            
            //apply spherical gravity to selected objects (set the objects in editor)
            foreach (GameObject o in objects)
            {
                if (o.GetComponent<Rigidbody>())
                {
                    o.GetComponent<Rigidbody>().AddForce((planet.transform.position - o.transform.position).normalized * gravitationalPull);
                }
            }
            //or apply gravity to all game objects with rigidbody
            //foreach (GameObject o in UnityEngine.Object.FindObjectsOfType<GameObject>())
            //{
            //    if (o.GetComponent<Rigidbody>() && o != planet)
            //    {
            //        o.GetComponent<Rigidbody>().AddForce((planet.transform.position - o.transform.position).normalized * gravitationalPull);
            //    }
            //}


            //couldn't get the atmosphere gravity working.
            //foreach (Collider collider in Physics.OverlapSphere(planet.transform.position, gravityRadius))
            //{
            //    if (collider.gameObject.GetComponent<Rigidbody>() && collider.gameObject != planet)
            //    {
            //        Vector3 gravDir = (collider.gameObject.transform.position - planet.transform.position).normalized;
            //        collider.gameObject.GetComponent<Rigidbody>().AddForce(gravDir * gravitationalPull);
            //    }
            //}
        }
    }

}
