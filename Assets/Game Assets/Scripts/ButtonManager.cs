﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {
    public AudioClip[] m_weebMusic;
    public AudioClip m_mainMenuMusic;
    private bool weebinOut = false;
    public AudioSource mainMenuAudio;
    public AudioSource weebAudio;

    public void NewGameBtn(string newGameLevel)
    {
        SceneManager.LoadScene(newGameLevel);
    }

    public void ExitGameBtn()
    {
        Application.Quit();
    }

    public void LetsWeeaboo()
    {
        if (!weebinOut)
        {
            int musicChoice = Random.Range(0, m_weebMusic.Length);
            mainMenuAudio.Pause();
            weebAudio.PlayOneShot(m_weebMusic[musicChoice]);
            StartCoroutine(WaitForMusicToEnd(m_weebMusic[musicChoice].length, mainMenuAudio, weebAudio));
            weebinOut = true;
        }
        else
        {
            weebinOut = false;
            weebAudio.Stop();
            mainMenuAudio.UnPause();
        }

    }

    IEnumerator WaitForMusicToEnd(float musicTime, AudioSource audioRestart, AudioSource audioStop)
    {
        yield return new WaitForSeconds(musicTime);
        weebinOut = false;
        audioStop.Stop();
        audioRestart.UnPause();
    }
}
