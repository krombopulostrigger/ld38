﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public GameObject p1;
    public TileMapGenerator tmGenr; private int[] tmDims;

    //construction parameters. will require refactoring
    public GameObject cantBuildHereXPrefab, canBuildHereSqrPrefab;//incidental prefabs
    public GameObject barricadePrefab, ghostBarricadePrefab;//building prefabs
	private int[] p1LastTgtAddr;
	private bool p1CouldBuild;
	private GameObject p1ConstrObjs;
	private List<GameObject> p1ConstrObjList;
    //end of construction parameters

	private PlayerController p1Controller;
	private bool p1Active;
	private bool p1Alive;

    public int cameraOffset = 15;

	// Use this for initialization
	void Start () {
        p1Active = true;
        if (p1Active)
        {
            p1Controller = p1.GetComponentInChildren<PlayerController>();
            p1Controller.setMapDims(50, 50);//this is bad hard coding - get rid of it later!
            //construction mode initialization - propagate to other players later
            p1ConstrObjList = new List<GameObject>();
            p1ConstrObjs = new GameObject();
            p1ConstrObjs.name = "P1ConstructionObjects";
            p1ConstrObjs.transform.parent = transform;
        }

        tmDims = tmGenr.getMapDims();
    }
	
	// Update is called once per frame
	void Update () {
        //player 1 setup
//        if (p1Active)
//        {
//            int[] myAddr = p1Controller.getMyAddress();
//            int[] myFacing = p1Controller.getMyFacingTile();
//
//            //buildModeStuff -- put in a separate method for MP cleanliness
//            if (p1Controller.IsInBuildMode())
//            {
//                //determine the position of the tile the player is targeting
//                tmDims = tmGenr.getMapDims();
//                int[] targetTile = new int[2];// p1Controller.getTargetTile();
//                myFacing = new int[2];
//                myFacing = p1Controller.getMyFacingTile();
//                targetTile[0] = myFacing[0]; targetTile[1] = myFacing[1];
//                int myWidth = tmDims[0]; int myHeight = tmDims[1]; int myXScale = tmDims[2]; int myYScale = tmDims[3]; int myZScale = tmDims[4];
//                Vector3 tgtTileSurfacePos = new Vector3(targetTile[0]*tmDims[2], 0.55f/*MAY NEED REFACTORING*/, targetTile[1]*tmDims[4]);//set target cellPos
//                //determine if anything even needs to change this frame
//                bool buildReqd = p1Controller.wasBuildRequested();
//                bool canBuildAtTarget = false;
//                if (targetTile[0] == -1 || targetTile[1] == -1) { p1Controller.buildComplete(); return; } //we're pointed at an invalid tile. don't try to continue
//                if (targetTile[0] == p1LastTgtAddr[0] && targetTile[1] == p1LastTgtAddr[1] && canBuildAtTarget == p1CouldBuild) { p1Controller.buildComplete(); return; }//we're pointed at the same place as before and in the same state. return
//                //determine whether we are able to build there
//                if (tmGenr.getObjAtAddr(targetTile[0], targetTile[1]) == null) { canBuildAtTarget = true; }
//
//                while (p1ConstrObjList.Count > 0)//start by getting rid of the old objects
//                {
//                    Object.Destroy(p1ConstrObjList[0]);
//                    p1ConstrObjList.RemoveAt(0);
//                }
//                //determine what the player is trying to build
//                int playerBuildMode = 1; //get this from the player later (0 is not build mode, 1 is barricade)
//                GameObject objToBuild = null; GameObject ghostObjToPreview = null;
//                switch (playerBuildMode)
//                {
//                    case 0:
//                        return;
//                    case 1:
//                        objToBuild = barricadePrefab;
//                        ghostObjToPreview = ghostBarricadePrefab;
//                        break;
//                    default:
//                        return;
//                }
//
//                //either we're pointing at a different tile, or the state has changed. now we'll make the objects we need
//                //if we can't build there (a building/character occupies that tile), show a Red X
//                if (!canBuildAtTarget)
//                {
//                    GameObject cantBuildHereXInst = Instantiate(cantBuildHereXPrefab);
//                    cantBuildHereXInst.transform.position = tgtTileSurfacePos;
//                    cantBuildHereXInst.transform.parent = p1ConstrObjs.transform;
//                    cantBuildHereXInst.name = "CantBuildThereX_Player1";
//                    p1ConstrObjList.Add(cantBuildHereXInst);
//                    p1Controller.buildComplete();
//                }
//                //if in build mode, build the thing
//                else if (buildReqd)
//                {
//                    GameObject lol = Instantiate(objToBuild);
//                    lol.transform.position = tgtTileSurfacePos;
//                    lol.transform.parent = p1ConstrObjs.transform;
//                    lol.name = "PlayerBldg";
//                    //tmGenr.putGameObjAtAddr(objToBuild, targetTile[0], targetTile[1]);
//                    tmGenr.buildingsOnMap[(targetTile[1]*tmDims[0])+targetTile[0]] = lol;
//                    p1Controller.buildComplete();
//                }
//                //if in preview mode, show the preview
//                else
//                {
//                    //draw a Green Square
//                    GameObject canBuildHereSqrInst = Instantiate(canBuildHereSqrPrefab);
//                    canBuildHereSqrInst.transform.position = tgtTileSurfacePos;
//                    canBuildHereSqrInst.transform.parent = p1ConstrObjs.transform;
//                    canBuildHereSqrInst.name = "CanBuildThereSqr_Player1";
//                    p1ConstrObjList.Add(canBuildHereSqrInst);
//                    
//                    //fill the tile with a ghost version of that object
//                    ghostObjToPreview = Instantiate(ghostBarricadePrefab);
//                    ghostObjToPreview.transform.position = tgtTileSurfacePos;
//                    ghostObjToPreview.transform.parent = p1ConstrObjs.transform;
//                    ghostObjToPreview.name = "PreviewBuildObj_Player1";
//                    p1ConstrObjList.Add(ghostObjToPreview);
//                }
//                //set the last target address to the current target address
//                p1LastTgtAddr = targetTile;
//                p1CouldBuild = canBuildAtTarget;
//            }
//            else//not in build mode. remove any silhouettes
//            {
//                while (p1ConstrObjList.Count > 0)//start by getting rid of the old objects
//                {
//                    Object.Destroy(p1ConstrObjList[0]);
//                    p1ConstrObjList.RemoveAt(0);
//                }
//            }
//        }
    }
		
}