﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : CharacterController
{
    private BoxCollider groundDetector;
    public Camera playerCam;
    float camRayLength = 100f;
    public int floorMask;

    private Vector3 mousePos;

    private bool isInBuildMode; private bool buildRequested;
    private bool buildModeKeyDown, buildModeKeyBounce;
    private bool buildKeyDown, buildKeyBounce;

    private bool m_reloading = false;

    public Gun m_gun;
    public Slider m_healthSlider;
    public Text m_ammoText;
    public Image m_damageImage;
    public Image m_reloadImage;

    public GameManager m_gameManager;

    // Use this for initialization
    public override void Start()
	{
		m_audioSource = GetComponent<AudioSource>();
        groundDetector = GetComponent<BoxCollider>();
		MAX_HEALTH = 100;
        health = MAX_HEALTH;

        forwardInput = strafeInput = 0;
        forwardVelocity = strafeVelocity = 0;

        isInBuildMode = false;
        buildRequested = false; buildKeyBounce = false;
		base.Start ();
		if (m_healthSlider != null) {
			m_healthSlider.maxValue = MAX_HEALTH;
			m_healthSlider.value = health;
        }
        m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

	public override void Update()
    {
		if (isDead ()) {
            return;
		}
        GetInput();
		if (m_ammoText != null) 
		{
			m_ammoText.text = m_gun.m_currentClipSize.ToString () + " | " + m_gun.m_totalAmmo.ToString ();
		}
        if (!m_canHit)
        {
            long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            if (currentTime - m_lastHit > m_hitDelay)
            {
                m_rend.material.color = m_normalColor;
                Color c = m_damageImage.color;
                c.a = 0;
                m_damageImage.color = c;
                m_canHit = true;
            }
        }
        if (Input.GetButtonDown("Reload"))
        {
            if(!m_reloading && m_gun.m_currentClipSize < m_gun.m_maxClipSize)
            {
                m_gun.Reload();
            }
        }
    }

    void FixedUpdate()
    {
        Move();
        Turn();
        getFacingAngle(rBody, tileAddress[0], tileAddress[1]);
    }

	public override void die()
	{
		//GAME OVER
		base.die();
        m_gameManager.SetGameOver();
    }

    public override void reduceHealth(float reduceBy)
    {
        if (m_canHit)
        {
			base.reduceHealth (reduceBy);
			m_healthSlider.value = health;
			Color c = m_damageImage.color;
			c.a = 0.25f;
			m_damageImage.color = c;
        }

    }

    public void ReloadAnimate(float reloadTime)
	{
		m_reloading = true;
		if (m_reloadImage != null) {
			Color c = m_reloadImage.color;
			c.a = 1;
			m_reloadImage.color = c;
			StartCoroutine(ReloadEndDelay(reloadTime));
		}
    }

    public void setMapDims(int widthIn, int heightIn)
    {

    }

    IEnumerator ReloadEndDelay(float reloadTime)
    {
        yield return new WaitForSeconds(reloadTime);
        Color c = m_reloadImage.color;
        c.a = 0;
        m_reloadImage.color = c;
        m_reloading = false;
    }

    protected void OnTriggerEnter(Collider col)
    {
        getMapAddress(col, rBody);
    }

    public void GetInput()
    {
        forwardInput = Input.GetAxis("Vertical");
        strafeInput = Input.GetAxis("Horizontal");

        UnityEngine.KeyCode buildReqKey = KeyCode.B;
        UnityEngine.KeyCode buildModeKey = KeyCode.V;

        //detect build mode input
        if(Input.GetKeyDown(buildModeKey) && !buildKeyBounce) { isInBuildMode = !isInBuildMode; buildKeyBounce = true; }
        else if(Input.GetKeyUp(buildModeKey) && buildKeyBounce) { buildKeyBounce = false; }

        //if in build mode, do build mode input
        if (isInBuildMode)
        {
            //build input processing
            if (Input.GetKeyDown(buildReqKey) && !buildRequested)
            {
                buildKeyBounce = true; buildRequested = true;
            }
            else if (Input.GetKeyUp(buildReqKey) && buildKeyBounce) { buildKeyBounce = false; }
        }
        //else { buildKeyBounce = false; }
    }

    public override void Move()
    {
		if (m_canMove) {
			if (Mathf.Abs (forwardInput) > inputDelay) {
				forwardVelocity = forwardInput * moveVelocity;
			} else
				forwardVelocity = 0;

			if (Mathf.Abs (strafeInput) > inputDelay) {
				strafeVelocity = strafeInput * moveVelocity;
			} else
				strafeVelocity = 0;

			rBody.velocity = new Vector3 (strafeVelocity, rBody.velocity.y, forwardVelocity);
		}
    }

    public override void Turn()
    {
		if (m_canMove) {
			
			RaycastHit groundHit;
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (camRay, out groundHit, camRayLength)) {
				Vector3 playerToMouse = groundHit.point - transform.position;
				playerToMouse.y = 0f;

				Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
				rBody.MoveRotation (newRotation);
			}
		}
    }

    public bool wasBuildRequested() { return buildRequested; }
    public void buildComplete() { buildRequested = false; }
    public bool IsInBuildMode() { return isInBuildMode; }


    public int[] getTargetTile()
    {
        //getFacingAngle(rBody, tileAddress[0], tileAddress[1]);
        return facingTile;
    }
}
