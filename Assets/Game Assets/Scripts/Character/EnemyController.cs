﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : CharacterController {

	public float m_sightRadius = 0f;

	public AudioClip m_attackSound;
	protected long m_lastAtkTime;
    
    private bool isCoroutineExecuting = false;
    public bool m_attack = false;

	public Transform m_goal;
	public Transform m_mapGoal;
	protected NavMeshAgent m_agent;
    public MeleeWeapon m_weapon;

    // Use this for initialization
    public override void Start () {
		m_audioSource = GetComponent<AudioSource>();
		m_lastAtkTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

		m_agent = GetComponent<NavMeshAgent> ();

		m_mapGoal = GameObject.FindGameObjectWithTag ("Goal").transform;
		m_goal = m_mapGoal;
		m_agent.destination = m_goal.position;

		m_weapon.setAttackSound (m_attackSound);

		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		if (isDead ()) {
			return;
		}
		m_agent.destination = new Vector3(m_goal.position.x, transform.position.y, m_goal.position.z);
		base.Update ();
        if (m_attack)
		{
            m_weapon.Swing();
        }
    }
		
	public void FixedUpdate() {
		lookForStuff ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player") || other.CompareTag("Goal")) //melee attack on player
        {
            m_attack = true;
		}
		//do melee attacks on structures

	}

    private void OnTriggerExit(Collider other)
    {
		if (other.CompareTag("Player") || other.CompareTag("Goal")) //melee attack on player
        {
            m_attack = false;
        }

        //do melee attacks on structures
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Enemy"))
        {
            rBody.isKinematic = true;
            m_playerColliderCheck = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Enemy"))
        {
            StartCoroutine(TryReAddPhysics());
        }
    }

    IEnumerator TryReAddPhysics()
    {
        //if (isCoroutineExecuting)
        //    yield break;

        //isCoroutineExecuting = true;

        m_playerColliderCheck = false;
        yield return new WaitForSeconds(5);

        if (!m_playerColliderCheck)
        {
            rBody.isKinematic = false;
        }
        //isCoroutineExecuting = false;
    }

	protected void lookForStuff()
	{
		if (!m_canMove)
			return;
		Vector3 myPosition = transform.position;
		Vector3 facingDirection = transform.forward;
		bool hitTarget = false;

		for (int angle = 0; angle <= 360; angle += 1) 
		{
			Vector3 direction =  Quaternion.AngleAxis(angle, transform.up) * facingDirection;
			Ray sightRay = new Ray(myPosition, direction);
			RaycastHit[] sightHits = Physics.RaycastAll(sightRay, m_sightRadius);
			foreach(RaycastHit hit in sightHits)
			{	
				if (hit.collider.gameObject.CompareTag("Player")) 
				{
					m_goal = hit.collider.gameObject.transform;
					hitTarget = true;
				}
				//if hit Goal then we do not care about player
				if (hit.collider.gameObject.CompareTag ("Goal")) {
					m_goal = hit.collider.gameObject.transform;
					hitTarget = true;
					break;
				}
			} 
		}
        if(m_mapGoal == null)
        {
            return;
        }
		else if (!hitTarget) {
			m_goal = m_mapGoal;
		}

	}

	public void setGoal(Transform goal)
	{
		m_mapGoal = goal;
	}

    public override void die()
	{
		//add experience and drop loot?
		//setGoal to current position
		m_agent.destination = transform.position;
		base.die ();
	}

}
