﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumbZombieController : EnemyController
{    
    // Use this for initialization
	public override void Start()
	{
		m_audioSource = GetComponent<AudioSource>();
        MAX_HEALTH = 10;
        health = MAX_HEALTH;

		base.Start ();
    }

	public override void die()
	{
		//add experience and drop loot?
		base.die ();
	}

    public override void Move()
    {
        //apply movements from controls
        float turnX = 0.25f;//Input.GetAxis("Horizontal");
        float moveZ = 0.5f;//Input.GetAxis("Vertical");

        transform.Rotate(0, 4f * turnX, 0);
        transform.Translate(0, 0, 0.005f * moveZ);
    }

}
