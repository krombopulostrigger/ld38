﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This will be the generic character class
 * All characters should have:
 * 1. Health
 * 2. Movement
 * 3. RigidBody
 * 
 * 
 * Note: the gun is the actual attack for the hero so only enemy classes
 * need an attack rate
**/

public abstract class CharacterController : MonoBehaviour {

	public AudioClip m_deathScream;

	public Rigidbody rBody;

    public float MAX_HEALTH;
    public float health;

    public float inputDelay = 0.1f;
    public float moveVelocity = 5f;

	public long m_hitDelay = 10;
    public long m_lastHit;
	public bool m_canHit = true;

	protected AudioSource m_audioSource;
	public Renderer m_rend;
	public Color m_normalColor;

    protected int[] tileAddress;
    protected int[] facingTile;
    protected static int[] mapDims;
    protected float forwardInput, strafeInput;
	protected float forwardVelocity, strafeVelocity;

	protected bool m_canMove = true;

    public bool m_playerColliderCheck = false;

    // Use this for initialization
    public virtual void Start () {
		m_audioSource = GetComponent<AudioSource>();
        tileAddress = new int[2]; tileAddress[0] = -1; tileAddress[1] = -1;
        facingTile = new int[2]; facingTile[0] = -1; facingTile[1] = -1;
        mapDims = new int[2]; mapDims[0] = 50; mapDims[1] = 50;//remove this hard-coding!!

        if (GetComponent<Rigidbody>())
            rBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("No rigidbody attached");

        forwardInput = strafeInput = 0;
        forwardVelocity = strafeVelocity = 0;

		m_rend = GetComponent<Renderer> ();
		m_normalColor = m_rend.material.color;
    }

    // Update is called once per frame
	public virtual void Update()
    {
		if (!m_canHit)
		{
			long currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			if (currentTime-m_lastHit > m_hitDelay) 
			{
				m_rend.material.color = m_normalColor;
				m_canHit = true;
			}
		}
    }

    public float getHealth()
	{
		return health;
	}

	public virtual void reduceHealth(float reduceBy)
	{
		if(m_canHit)
		{
			health -= reduceBy;
			m_rend.material.color = Color.red;
			//added isDead to prevent extra dieing on collisions after death
			if (health <= 0 && !isDead()) 
			{
				die ();
			}
			m_lastHit = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			m_canHit = false;
		}
	}

    public virtual void die()
	{
		m_audioSource.PlayOneShot (m_deathScream, 10);
		m_canMove = false;
		updateLayers(gameObject, LayerMask.NameToLayer ("Dead"));
		//Remove from game
		Destroy (gameObject, 1f);
	}

	public void updateLayers(GameObject obj, int newLayer)
	{
		obj.layer = newLayer;

		foreach (Transform child in obj.transform) {
			updateLayers (child.gameObject, newLayer);
		}
	}

    public int[] getMyAddress()
    {
        return tileAddress;
    }

    public int[] getMyFacingTile()
    {
        return facingTile;
    }

    public void getFacingAngle(Rigidbody myRbIn, int xPosIn, int yPosIn)
    {
        //figure out the tile we're facing
        //Rigidbody myRb = GetComponent<Rigidbody>();
        Vector3 myEulerAngle = myRbIn.transform.eulerAngles;
        while (myEulerAngle[1] < 0) { myEulerAngle[1] += 360; }
        myEulerAngle[1] = myEulerAngle[1] % 360;
        //|7|0|1|//
        //|6|P|2|//-With player 'P'  in centre, tiles go like that
        //|5|4|3|//-X increases L->R, Y increases bottom->top
        if (myEulerAngle[1] >= 337.5 || myEulerAngle[1] < 22.5)//0
        {
            facingTile[0] = tileAddress[0];
            facingTile[1] = (tileAddress[1] + 1 < mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
        }
        else if (myEulerAngle[1] >= 22.5 && myEulerAngle[1] < 67.5)//1
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) + 1 < mapDims[0]) ? (tileAddress[0] + 1) : -1;//we could be looking off the right-hand side of the map
            facingTile[1] = (tileAddress[1] + 1 < mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
        }
        else if (myEulerAngle[1] >= 67.5 && myEulerAngle[1] < 112.5)//2
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) + 1 < mapDims[0]) ? (tileAddress[0] + 1) : -1;//we could be looking off the right-hand side of the map
            facingTile[1] = tileAddress[1];
        }
        else if (myEulerAngle[1] >= 112.5 && myEulerAngle[1] < 157.5)//3
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) + 1 < mapDims[0]) ? (tileAddress[0] + 1) : -1;//we could be looking off the right-hand side of the map
            facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
        }
        else if (myEulerAngle[1] >= 157.5 && myEulerAngle[1] < 202.5)//4
        {
            facingTile[0] = tileAddress[0];
            facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
        }
        else if (myEulerAngle[1] >= 202.5 && myEulerAngle[1] < 247.5)//5
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) - 1 >= 0) ? tileAddress[0] - 1 : -1;//we could be looking off the left-hand side of the map
            facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
        }
        else if (myEulerAngle[1] >= 247.5 && myEulerAngle[1] < 292.5)//6
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) - 1 >= 0) ? tileAddress[0] - 1 : -1;//we could be looking off the left-hand side of the map
            facingTile[1] = tileAddress[1];
        }
        else//7
        {
            facingTile[0] = ((tileAddress[0] % mapDims[0]) - 1 >= 0) ? tileAddress[0] - 1 : -1;//we could be looking off the left-hand side of the map
            facingTile[1] = (tileAddress[1] + 1 < mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
        }
    }

    public void getMapAddress(Collider other, Rigidbody myRb)
    {
        if (other.CompareTag("GroundTile"))
        {
            tileAddress = new int[2]; tileAddress[0] = -1; tileAddress[1] = -1;
            facingTile = new int[2]; facingTile[0] = -1; facingTile[1] = -1;
            mapDims = new int[2]; mapDims[0] = 50; mapDims[1] = 50;//remove this hard-coding!!
            //get the position to set our address - convert this to not using colliders later. this can be done using position and map tile scale
            String otherName = other.name;
            String[] tmpName = otherName.Split('[');
            otherName = tmpName[1];
            tmpName = otherName.Split(',');
            int testX = -1; int testY = -1;
            Int32.TryParse(tmpName[0], out testX);
            tmpName = tmpName[1].Split(']');
            Int32.TryParse(tmpName[0], out testY);
            tileAddress[0] = testX;
            tileAddress[1] = testY;

            getFacingAngle(myRb, tileAddress[0], tileAddress[1]);
            /*
            //figure out the tile we're facing
            //Rigidbody myRb = GetComponent<Rigidbody>();
            Vector3 myEulerAngle = myRb.transform.eulerAngles;
            while(myEulerAngle[1] < 0) { myEulerAngle[1] += 360; }
            myEulerAngle[1] = myEulerAngle[1] % 360;
            //|7|0|1|//
            //|6|P|2|//-With player 'P'  in centre, tiles go like that
            //|5|4|3|//-X increases L->R, Y increases bottom->top
            if (myEulerAngle[1] >= 337.5 || myEulerAngle[1] < 22.5)//0
            {
                facingTile[0] = tileAddress[0];
                facingTile[1] = (tileAddress[1]+1<mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
            }
            else if (myEulerAngle[1] >= 22.5 && myEulerAngle[1] < 67.5)//1
            {
                facingTile[0] = ((tileAddress[0]%mapDims[0])+1 < mapDims[0]) ? (tileAddress[0]+1) : -1;//we could be looking off the right-hand side of the map
                facingTile[1] = (tileAddress[1] + 1 < mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
            }
            else if (myEulerAngle[1] >= 67.5 && myEulerAngle[1] < 112.5)//2
            {
                facingTile[0] = ((tileAddress[0] % mapDims[0]) + 1 < mapDims[0]) ? (tileAddress[0] + 1) : -1;//we could be looking off the right-hand side of the map
                facingTile[1] = tileAddress[1];
            }
            else if (myEulerAngle[1] >= 112.5 && myEulerAngle[1] < 157.5)//3
            {
                facingTile[0] = ((tileAddress[0] % mapDims[0]) + 1 < mapDims[0]) ? (tileAddress[0] + 1) : -1;//we could be looking off the right-hand side of the map
                facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
            }
            else if (myEulerAngle[1] >= 157.5 && myEulerAngle[1] < 202.5)//4
            {
                facingTile[0] = tileAddress[0];
                facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
            }
            else if (myEulerAngle[1] >= 202.5 && myEulerAngle[1] < 247.5)//5
            {
                facingTile[0] = ((tileAddress[0]%mapDims[0])-1 >= 0) ? tileAddress[0]-1 : -1;//we could be looking off the left-hand side of the map
                facingTile[1] = tileAddress[1] - 1;//we could be looking past the bottom (min y-dim) of the map (just use -1 in the bad case)
            }
            else if (myEulerAngle[1] >= 247.5 && myEulerAngle[1] < 292.5)//6
            {
                facingTile[0] = ((tileAddress[0] % mapDims[0]) - 1 >= 0) ? tileAddress[0] - 1 : -1;//we could be looking off the left-hand side of the map
                facingTile[1] = tileAddress[1];
            }
            else//7
            {
                facingTile[0] = ((tileAddress[0] % mapDims[0]) - 1 >= 0) ? tileAddress[0] - 1 : -1;//we could be looking off the left-hand side of the map
                facingTile[1] = (tileAddress[1] + 1 < mapDims[1]) ? tileAddress[1] + 1 : -1; //we could be looking past the top (max y-dim) of the map
            }
            */
        }
    }

    public virtual void Move()
    {
		if(m_canMove)
			rBody.velocity = new Vector3(0, rBody.velocity.y, moveVelocity);
    }

	public virtual void Turn()
    {
		
    }

	public bool isDead()
	{
		return gameObject.layer == LayerMask.NameToLayer("Dead");
	}
}
